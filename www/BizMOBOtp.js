var exec = require('cordova/exec');

var BizMOBOtp = {
    registerOtpAuth : function (param, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "BizMOBOtp", "registerOtpAuth", [param]);
    },
    requestOtpAuth : function (param, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "BizMOBOtp", "requestOtpAuth", [param]);
    }
};

module.exports = BizMOBOtp;



