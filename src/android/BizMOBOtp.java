package com.mcnc.bizmob.plugin.otp;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.mcnc.bizmob.plugin.otp.authenticator.AccountDb;
import com.mcnc.bizmob.plugin.otp.authenticator.OtpSource;
import com.mcnc.bizmob.plugin.otp.authenticator.OtpSourceException;
import com.mcnc.bizmob.plugin.otp.authenticator.TotpClock;
import com.mcnc.bizmob.plugin.otp.authenticator.TotpCountdownTask;
import com.mcnc.bizmob.plugin.otp.authenticator.TotpCounter;
import com.mcnc.bizmob.plugin.otp.authenticator.Utilities;
import com.mcnc.bizmob.plugin.otp.authenticator.ability.DependencyInjector;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * This class echoes a string called from JavaScript.
 */
public class BizMOBOtp extends CordovaPlugin {

    private CallbackContext mCallbackContext;

    private final String TAG = "OTPAuthPlugin";
    private static final String ACTION_REGISTER_OTP_AUTH = "registerOtpAuth";
    private static final String ACTION_REQUEST_OTP_AUTH = "requestOtpAuth";

    private static final String PARAM_HEADER = "header";
    private static final String PARAM_BODY = "body";

    private JSONObject resultData = null;
    private JSONObject resultHeader = null;
    private JSONObject resultBody = null;
    private JSONObject param = null;

    private JSONObject header = null;
    private JSONObject body = null;

    protected Handler handler = new Handler(Looper.getMainLooper());

    /**
     * Frequency (milliseconds) with which TOTP countdown indicators are updated.
     */
    private static final long TOTP_COUNTDOWN_REFRESH_PERIOD = 100;

    /**
     * Counter used for generating TOTP verification codes.
     */
    private TotpCounter mTotpCounter;

    /**
     * Clock used for generating TOTP verification codes.
     */
    private TotpClock mTotpClock;
    /**
     * Task that periodically notifies this activity about the amount of time remaining until
     * the TOTP codes refresh. The task also notifies this activity when TOTP codes refresh.
     */
    private static TotpCountdownTask mTotpCountdownTask;

    /**
     * Phase of TOTP countdown indicators. The phase is in {@code [0, 1]} with {@code 1} meaning
     * full time step remaining until the code refreshes, and {@code 0} meaning the code is refreshing
     * right now.
     */
    private double mRemainTotpCountdown;
    private AccountDb mAccountDb;
    private OtpSource mOtpProvider;

    private String mUserId;
    private boolean isBasedOnUserId = true;




    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        mCallbackContext = callbackContext;

        resultData = new JSONObject();
        resultHeader = new JSONObject();
        resultBody = new JSONObject();

        resultData.put(PARAM_HEADER, resultHeader);
        resultData.put(PARAM_BODY, resultBody);

        param = args.getJSONObject(0);

        if(param.has(PARAM_HEADER)){
            header = param.getJSONObject(PARAM_HEADER);
        }
        if(param.has(PARAM_BODY)){
            body = param.getJSONObject(PARAM_BODY);
        }

        if (action.equals(ACTION_REGISTER_OTP_AUTH)) {
            initAccountProvider();
            registerOTPAuth();
            return true;
        } else if (action.equals(ACTION_REQUEST_OTP_AUTH)) {
            initAccountProvider();

            return true;
        } else {
            resultHeader.put("result", false);
//            resultHeader.put("apiName", "UPDATE");
//            resultHeader.put("language", false);
//            resultHeader.put("osType", "android");
//            resultHeader.put("displayType", "phone");
            resultHeader.put("errorCode", "NE0007");
            resultHeader.put("errorText", "INVALID_ACTION");

            sendCallback(resultData);
            return true;

        }
    }

    private void initAccountProvider() {
        DependencyInjector.configureForProductionIfNotConfigured(cordova.getActivity());

        mAccountDb = DependencyInjector.getAccountDb();
        mOtpProvider = DependencyInjector.getOtpProvider();

        mTotpCounter = mOtpProvider.getTotpCounter();
        mTotpClock = mOtpProvider.getTotpClock();
    }

    private void registerOTPAuth() throws JSONException {
            String name = body.getString("name");
            String secret_key = body.getString("secret_key");
            String issuer = body.getString("issuer");

            validate:
            {
                if (AccountDb.getSigningOracle(secret_key) == null) {
                    Log.e(TAG, "Invalid Secret Key");
                    resultHeader.put("result", false);
//                    resultHeader.put("apiName", "UPDATE");
//                    resultHeader.put("language", false);
//                    resultHeader.put("osType", "android");
//                    resultHeader.put("displayType", "phone");
                    resultHeader.put("errorCode", "OTPE0003");
                    resultHeader.put("errorText", "Invalid Secret Key");
                    break validate;
                }

                if (mUserId.equals(mAccountDb.getUserId(name)) &&
                        secret_key.equals(mAccountDb.getSecret(name)) &&
                        mAccountDb.getCounter(name).equals(AccountDb.DEFAULT_HOTP_COUNTER) &&
                        mAccountDb.getIssuer(name).equals(issuer) &&
                        mAccountDb.getType(name) == AccountDb.OtpType.TOTP) {

                    resultHeader.put("result", false);
//                    resultHeader.put("apiName", "UPDATE");
//                    resultHeader.put("language", false);
//                    resultHeader.put("osType", "android");
//                    resultHeader.put("displayType", "phone");
                    resultHeader.put("errorCode", "OTPE0004");
                    resultHeader.put("errorText", "Nothing to update, account is already added");
                    break validate;
                }

                // Delete all data in purpose to store only single account
                // mAccountDb.deleteAllData();
                if (!saveSecret(mUserId, name, secret_key, null, issuer, AccountDb.OtpType.TOTP, 0)) {
                    Log.e(TAG, "Failed to save secret key.");
                    resultHeader.put("result", false);
//                    resultHeader.put("apiName", "UPDATE");
//                    resultHeader.put("language", false);
//                    resultHeader.put("osType", "android");
//                    resultHeader.put("displayType", "phone");
                    resultHeader.put("errorCode", "OTPE0005");
                    resultHeader.put("errorText", "Failed to save secret key");
                    break validate;
                }

                resultHeader.put("result", true);
                resultHeader.put("message","Account is successful added");
//                resultHeader.put("apiName", "UPDATE");
//                resultHeader.put("language", false);
//                resultHeader.put("osType", "android");
//                resultHeader.put("displayType", "phone");
                resultHeader.put("errorCode", "");
                resultHeader.put("errorText", "");
            }

            sendCallback(resultData);
    }

    private void requestCode() throws JSONException {
            final boolean enable = body.getBoolean("enable");

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (enable) {
                        updateCodesAndStartTotpCountdownTask();
                    } else {
                        stopTotpCountdownTask();

                        try {
                            resultHeader.put("result", true);
                            resultHeader.put("message","Terminated PIN generation.");
//                resultHeader.put("apiName", "UPDATE");
//                resultHeader.put("language", false);
//                resultHeader.put("osType", "android");
//                resultHeader.put("displayType", "phone");
                            resultHeader.put("errorCode", "");
                            resultHeader.put("errorText", "");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        sendCallback(resultData);
                    }
                }
            });
    }

    /**
     * Saves the secret key to local storage on the phone.
     *
     * @param user         the user email address. When editing, the new user email.
     * @param secret       the secret key
     * @param originalUser If editing, the original user email, otherwise null.
     * @param type         hotp vs totp
     * @param counter      only important for the hotp type
     * @return {@code true} if the secret was saved, {@code false} otherwise.
     */
    private boolean saveSecret(String userId, String user, String secret,
                               String originalUser, String issuer, AccountDb.OtpType type, Integer counter) {
        if (originalUser == null) {  // new user account
            originalUser = user;
        }
        if (secret != null) {
            AccountDb accountDb = DependencyInjector.getAccountDb();
            accountDb.update(userId, user, secret, originalUser, issuer, type, counter);
            return true;
        } else {
            Log.e(TAG, "Trying to save an empty secret key");
            return false;
        }
    }

    private void updateCodesAndStartTotpCountdownTask() {
        stopTotpCountdownTask();

        mTotpCountdownTask =
                new TotpCountdownTask(mTotpCounter, mTotpClock, TOTP_COUNTDOWN_REFRESH_PERIOD);
        mTotpCountdownTask.setListener(new TotpCountdownTask.Listener() {
            @Override
            public void onTotpCountdown(long millisRemaining) {
                if (cordova.getActivity() == null || cordova.getActivity().isFinishing()) {
                    // No need to reach to this even because the Activity is finishing anyway
                    stopTotpCountdownTask();
                    return;
                }

                mRemainTotpCountdown = Utilities.millisToSeconds(millisRemaining);
            }

            @Override
            public void onTotpCounterValueChanged() {
                if (cordova.getActivity() == null || cordova.getActivity().isFinishing()) {
                    // No need to reach to this even because the Activity is finishing anyway
                    return;
                }

                refreshVerificationCodes();
            }
        });

        mTotpCountdownTask.startAndNotifyListener();
    }

    private void stopTotpCountdownTask() {
        if (mTotpCountdownTask != null) {
            mTotpCountdownTask.stop();
            mTotpCountdownTask = null;
        }
    }

    private void refreshVerificationCodes() {
        if (!refreshUser()) {
            stopTotpCountdownTask();

            JSONObject resultData = new JSONObject();
            try {
                resultHeader.put("result", false);
//                resultHeader.put("apiName", "UPDATE");
//                resultHeader.put("language", false);
//                resultHeader.put("osType", "android");
//                resultHeader.put("displayType", "phone");
                resultHeader.put("errorCode", "");
                resultHeader.put("errorText", "There is no user registered.");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendCallback(resultData);
        }
    }

    private boolean refreshUser() {
        ArrayList<String> usernames = new ArrayList<>();
        mAccountDb.getNames(usernames);

        int userCount = usernames.size();

        try {
            if (userCount > 0) {
                if (isBasedOnUserId) {
                    PinInfo user = new PinInfo();
                    user.user = mAccountDb.getEmail(mUserId);
                    if (user.user != null) {
                        user.issuer = mAccountDb.getIssuer(user.user);
                        JSONObject resultData = new JSONObject();
                        JSONObject data = computePinAndCallback(user, false);

                        resultHeader.put("result", true);
                        resultHeader.put("data", data);
//                resultHeader.put("apiName", "UPDATE");
//                resultHeader.put("language", false);
//                resultHeader.put("osType", "android");
//                resultHeader.put("displayType", "phone");
                        resultHeader.put("errorCode", "");
                        resultHeader.put("errorText", "");
                        sendCallback(resultData);
                    } else {
                        return false;
                    }
                } else {
                    JSONObject resultData = new JSONObject();
                    JSONArray dataList = new JSONArray();
                    for (String username : usernames) {
                        PinInfo user = new PinInfo();
                        user.user = username;
                        user.issuer = mAccountDb.getIssuer(user.user);

                        JSONObject data = computePinAndCallback(user, false);
                        dataList.put(data);
                    }

                    resultHeader.put("result", true);
                    resultHeader.put("data_list", dataList);
//                resultHeader.put("apiName", "UPDATE");
//                resultHeader.put("language", false);
//                resultHeader.put("osType", "android");
//                resultHeader.put("displayType", "phone");
                    resultHeader.put("errorCode", "");
                    resultHeader.put("errorText", "");
                    sendCallback(resultData);
                }
                return true;
            }
        } catch (OtpSourceException ignored) {
            ignored.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private JSONObject computePinAndCallback(PinInfo user, boolean computeHotp) throws OtpSourceException {
        if (user == null) {
            return null;
        }

        AccountDb.OtpType type = mAccountDb.getType(user.user);
        user.isHotp = (type == AccountDb.OtpType.HOTP);

        if (!user.isHotp || computeHotp) {
            // Always safe to recompute, because this code path is only
            // reached if the account is:
            // - Time-based, in which case getNextCode() does not change state.
            // - Counter-based (HOTP) and computeHotp is true.
            user.pin = mOtpProvider.getNextCode(user.user);
        }

        JSONObject data = new JSONObject();
        try {
            data.put("name", user.user);
            data.put("pin", user.pin);
            data.put("remains_time", mRemainTotpCountdown);
            data.put("issuer", user.issuer);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * A tuple of user, OTP value, and type, that represents a particular user.
     *
     * @author uyphanha@mcnc.co.kr (Phanah UY)
     */
    private static class PinInfo {
        private String pin; // calculated OTP, or a placeholder if not calculated
        private String user;
        private String issuer;
        private boolean isHotp = false; // used to see if button needs to be displayed
    }

    private JSONObject createErrorResult(String errorMsg, String errorCode) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", false);
        jsonObject.put("error_message", errorMsg);
        jsonObject.put("error_code", errorCode);
        return jsonObject;
    }

    private void sendCallback(JSONObject result) {
        try {
            if (result.getJSONObject(PARAM_HEADER).getBoolean("result")) {
                mCallbackContext.success(result);
            } else {
                mCallbackContext.error(result);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
